package com.example.demo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;


@Service
public interface CourseDetailsDAO extends CrudRepository<CourseDetailsBean, String> {

	//public long deleteByName(String name);
}
