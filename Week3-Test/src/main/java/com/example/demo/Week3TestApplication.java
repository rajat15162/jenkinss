package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Week3TestApplication {

	public static void main(String[] args) {
		SpringApplication.run(Week3TestApplication.class, args);
	}

}
