package com.example.demo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CourseDetails")
public class CourseDetailsBean {
	
	@Id
	private String cid;
	@Column
	private String ctitle;
	@Override
	public String toString() {
		return "CourseDetailsBean [cid=" + cid + ", ctitle=" + ctitle + ", sdate=" + sdate + ", edate=" + edate
				+ ", fees=" + fees + "]";
	}
	@Column
	private String sdate;
	@Column
	private String edate;
	@Column
	private String fees;
	public String getCid() {
		return cid;
	}
	public void setCid(String cid) {
		this.cid = cid;
	}
	public String getCtitle() {
		return ctitle;
	}
	public void setCtitle(String ctitle) {
		this.ctitle = ctitle;
	}
	public String getSdate() {
		return sdate;
	}
	public void setSdate(String sdate) {
		this.sdate = sdate;
	}
	public String getEdate() {
		return edate;
	}
	public void setEdate(String edate) {
		this.edate = edate;
	}
	public String getFees() {
		return fees;
	}
	public void setFees(String fees) {
		this.fees = fees;
	}
	public CourseDetailsBean(String cid, String ctitle, String sdate, String edate, String fees) {
		super();
		this.cid = cid;
		this.ctitle = ctitle;
		this.sdate = sdate;
		this.edate = edate;
		this.fees = fees;
	}
	public CourseDetailsBean() {
		super();
		
	}
	
	
	
	

}
