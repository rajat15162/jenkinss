package com.example.demo;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/courses")
@Transactional
public class Controller {

	
	@Autowired
	CourseDetailsDAO courseDetailsDAO;
	
	@PostMapping("/add")
	public String addCourse(@RequestBody CourseDetailsBean ob) {

		
		courseDetailsDAO.save(ob);
		return "Course Added";
		
		
	}
	
	
	@GetMapping("/list")
	public List<CourseDetailsBean> display(){
		
		
		return (List<CourseDetailsBean>)courseDetailsDAO.findAll();
	}
	
	
	
	
	@PostMapping("/deleteById")
	public String deleteCourseById(@RequestBody String id)
	{
		if(courseDetailsDAO.existsById(id)){
			courseDetailsDAO.deleteById(id);
			return "Course Deleted";
			
		}
		
		else
			return "id not present";
		
	}
	
	@PostMapping("/searchById")
	public Optional<CourseDetailsBean> find(@RequestBody String id) {
		
		return courseDetailsDAO.findById(id);
		
	}
	 
	 
	
}
