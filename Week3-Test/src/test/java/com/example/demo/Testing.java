package com.example.demo;



import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;



public class Testing 
{      
	
	static CourseDetailsDAO courseDetailsDAO;
	
	@Test
	public void testAddCourse() {         
		
		String URL_CREATE_EMPLOYEE = "http://localhost:8000/courses/add";    
		CourseDetailsBean newCourse = new CourseDetailsBean("45","gg","gg","ggfg","66");
		
		          
		HttpHeaders headers = new HttpHeaders();          
		headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);         
		headers.setContentType(MediaType.APPLICATION_JSON);        
		RestTemplate restTemplate = new RestTemplate();       
		// Data attached to the request.      
		HttpEntity<CourseDetailsBean> requestBody = new HttpEntity<>(newCourse, headers);     
		// Send request with POST method.     
		String e = restTemplate.postForObject(URL_CREATE_EMPLOYEE, requestBody, String.class);  
		//System.out.println(e);
		//System.out.println(e);
		//List<CourseDetailsBean> l = (List<CourseDetailsBean>)courseDetailsDAO.findAll();
		//System.out.println(l);
		assertEquals("Course Added", e);
		
		
		
		
	}
	
	@Test  
	public void testDeleteCourseForwhichIdNotPresent () {
		 String URL_CREATE_EMPLOYEE = "http://localhost:8000/courses/deleteById";    
		String id ="33";             // non-existing id     
		HttpHeaders headers = new HttpHeaders();          
		headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);         
		headers.setContentType(MediaType.APPLICATION_JSON);        
		RestTemplate restTemplate = new RestTemplate();       
		// Data attached to the request.      
		HttpEntity<String> requestBody = new HttpEntity<>(id, headers);     
		// Send request with POST method.     
		String e = restTemplate.postForObject(URL_CREATE_EMPLOYEE, requestBody, String.class);   
		System.out.println(e);
		assertEquals("id not present", e);
	}
	
	
	@Test  // 
	public void testSearchForPresent() {
		 String URL_CREATE_EMPLOYEE = "http://localhost:8000/courses/searchById";    
		String id ="1";                 
		HttpHeaders headers = new HttpHeaders();          
		headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);         
		headers.setContentType(MediaType.APPLICATION_JSON);        
		RestTemplate restTemplate = new RestTemplate();       
		// Data attached to the request.      
		HttpEntity<String> requestBody = new HttpEntity<>(id, headers);     
		// Send request with POST method.     
		CourseDetailsBean e = restTemplate.postForObject(URL_CREATE_EMPLOYEE, requestBody, CourseDetailsBean.class);   
		//System.out.println(e);
		assertNotNull(e);
	}
	
	@Test  // 
	public void testSearchForNotPresentId() {
		 String URL_CREATE_EMPLOYEE = "http://localhost:8000/courses/searchById";    
		String id ="55";                 
		HttpHeaders headers = new HttpHeaders();          
		headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);         
		headers.setContentType(MediaType.APPLICATION_JSON);        
		RestTemplate restTemplate = new RestTemplate();       
		// Data attached to the request.      
		HttpEntity<String> requestBody = new HttpEntity<>(id, headers);     
		// Send request with POST method.     
		CourseDetailsBean e = restTemplate.postForObject(URL_CREATE_EMPLOYEE, requestBody, CourseDetailsBean.class);   
		//System.out.println(e);
		assertNull(e);
	}
	
	
	/*public static void main(String[] args) {
		
		String URL_CREATE_EMPLOYEE = "http://localhost:8000/courses/list";    
		   HttpHeaders headers = new HttpHeaders();

	       headers.setAccept(Arrays.asList(new MediaType[] { MediaType.APPLICATION_JSON }));

	       // Request to return JSON format
	       headers.setContentType(MediaType.APPLICATION_JSON);
	      // headers.set("my_other_key", "my_other_value");

	       // HttpEntity<String>: To get result as String.
	       HttpEntity<CourseDetailsBean[]> entity = new HttpEntity<CourseDetailsBean[]>(headers);

	       // RestTemplate
	       RestTemplate restTemplate = new RestTemplate();

	       // Send request with GET method, and Headers.
	       ResponseEntity<CourseDetailsBean[]> response = restTemplate.exchange(URL_CREATE_EMPLOYEE, //
	               HttpMethod.GET, entity, CourseDetailsBean[].class);

	       CourseDetailsBean[] result = response.getBody();
	       
	      List<CourseDetailsBean>l=  Arrays.asList(result).stream().collect(Collectors.toList());
	      
	      for(CourseDetailsBean ob : result) {
	    	    System.out.println(ob.getCid());
	    	}
	
	
		
	}*/
	
	
	/*@Test
	public void testDeleteCourse() {
		 String URL_CREATE_EMPLOYEE = "http://localhost:8000/courses/deleteById";    
		String id ="33";                  
		HttpHeaders headers = new HttpHeaders();          
		headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);         
		headers.setContentType(MediaType.APPLICATION_JSON);        
		RestTemplate restTemplate = new RestTemplate();       
		// Data attached to the request.      
		HttpEntity<String> requestBody = new HttpEntity<>(id, headers);     
		// Send request with POST method.     
		String e = restTemplate.postForObject(URL_CREATE_EMPLOYEE, requestBody, String.class);   
		System.out.println(e);
		assertEquals("Course Deleted", e);
	}*/
	
	
	
	
	
	
}